
template <typename T>
List<T>::const_iterator::const_iterator():current{nullptr}// default zero parameter constructor
{}

template <typename T>
const T & List<T>::const_iterator::operator*() const // operator*() to return element
{
	return retrieve();
}

// increment/decrement operators
template <typename T>
 typename List<T>::const_iterator& List<T>::const_iterator::operator++()//Prefix Increment
{
	current = current -> next;
	return *this;
}

template <typename T>
typename List<T>::const_iterator List<T>::const_iterator::operator++(int)//Postfix Increment
{
	const_iterator old = *this;
	++(*this);
	return old;
}

template <typename T>
typename List<T>::const_iterator& List<T>::const_iterator::operator--()//Prefix Decrement
{
	 current = current -> prev;
	return *this;
}

template <typename T>
typename List<T>::const_iterator List<T>::const_iterator::operator--(int)//Postfix Decrement
{
	const_iterator old = *this;
	--(*this);
	return old;
}


// comparison operators
template <typename T>
bool List<T>::const_iterator::operator==(const const_iterator &rhs) const
{
	return current == rhs.current;
}

template <typename T>
bool List<T>::const_iterator::operator!=(const const_iterator &rhs) const
{
	return !(*this == rhs);
}

template <typename T>
T & List<T>::const_iterator::retrieve() const // retrieve the element refers to
{
	return current -> data;
}

template <typename T>
List<T>::const_iterator::const_iterator(Node *p):current{p} // protected constructor
{}


template <typename T>
typename List<T>::iterator iterator()
{}

template <typename T>
T & List<T>::iterator::operator*()
{
	return const_iterator::retrieve();
}

template <typename T>
const T & List<T>::iterator::operator*() const
{
	return const_iterator::operator*();
}

// increment/decrement operators
template <typename T>
typename List<T>::iterator& List<T>::iterator::operator++()
{
	this -> current = this -> current -> next;
	return *this;
}


template <typename T>
typename List<T>::iterator List<T>::iterator::operator++(int)
{
	iterator old = *this;
	++(*this);
	return old;
}

template <typename T>
typename List<T>::iterator& List<T>::iterator::operator--()
{
	this -> current = this -> current -> prev;
	return *this;
}

template <typename T>
typename List<T>::iterator List<T>::iterator::operator--(int)
{
	iterator old = *this;
	--(*this);
	return old;
}

template <typename T>
List<T>::iterator::iterator(Node *p):const_iterator{p}
{}

// constructor, desctructor, copy constructor
template <typename T>
List<T>::List() // default zero parameter constructor
{
	init();
}

template <typename T>
List<T>::List(const List &rhs) // copy constructor
{
	init();
	for(auto &x : rhs)
		push_back(x);
}

template <typename T>
List<T>::List(List && rhs):theSize{rhs.theSize}, head{rhs.head}, tail{rhs.tail} // move constructor
{
	rhs.theSize = 0;
	rhs.head = nullptr;
	rhs.tail = nullptr;
	
}

// num elements with value of val
template <typename T>
List<T>::List(int num, const T& val)
{
	init();

	for(int count = 0;count < num; count++)
	{
		push_back(val);
	}
}

// constructs with elements [start, end)
template <typename T>
List<T>::List(const_iterator start, const_iterator end)
{
	init();
	
	for(; start != end; start++)
		push_back(*start);
	
}

template <typename T>
List<T>::~List() // destructor
{
	clear();
	delete head;
	delete tail;
}
 
template <typename T>
void List<T>::init()
{
	theSize = 0;
	head = new Node;
	tail = new Node;
	head -> next = tail;
	tail -> prev = head;

}

// copy assignment operator
template <typename T>
const List<T>& List<T>::operator=(const List<T> &rhs)
{
	std::swap(theSize, rhs.theSize);
	std::swap(head, rhs.head);
	std::swap(tail, rhs.tail);
}

// move assignment operator
template <typename T>
List<T>& List<T>::operator=(List && rhs)
{
	theSize = std::move(rhs.theSize);
	head = std::move(rhs.head);
	tail = std::move(rhs.tail);
	return *this;
}

// member functions
template <typename T>
int List<T>::size() const // number of elements
{
	return theSize;
}

template <typename T>
bool List<T>::empty() const // check if list is empty
{
	return (theSize ==0);
}

template <typename T>
void List<T>::clear() // delete all elements
{
	while(!empty())
	{
		pop_front();
	}
}


template <typename T>
void List<T>::reverse() // reverse the order of the elements
{
	Node *temp = new Node;
	temp = nullptr;
	Node *current = new Node;
	current = head;
	
	while(current != nullptr)
	{
		temp = current -> prev;
		current -> prev  = current -> next;
		current -> next = temp;
		current = current -> prev;
	}
		
	Node *tempTail = tail;
	tail = head;
	head = tempTail;
	
	if(temp != nullptr)
		head = temp -> prev;
}


template <typename T>
T &List<T>::front() // reference to the first element
{
	return *begin();
}


template <typename T>
const T& List<T>::front() const
{
	return *begin();
}


template <typename T>
T &List<T>::back() // reference to the last element
{
	return --(*end());
}


template <typename T>
const T & List<T>::back() const
{
	return --*end();
}


template <typename T>
void List<T>::push_front(const T & val) // insert to the beginning
{
	insert(begin(), val);
}


template <typename T>	
void List<T>::push_front(T && val) // move version of insert
{
	insert(begin(), std::move(val));
}


template <typename T>
void List<T>::push_back(const T & val) // insert to the end
{
	insert(end(), val);
}


template <typename T>
void List<T>::push_back(T && val) // move version of insert
{
	insert(end(), std::move(val));
}


template <typename T>
void List<T>::pop_front() // delete first element
{
	erase(begin());
}


template <typename T>
void List<T>::pop_back() // delete last element
{
	erase( --end( ) );
}


template <typename T>
void List<T>::remove(const T &val) // remove all elements with value = val
{
	for(auto it = this->begin(); it != this ->end(); it++)
	{
		if(*it == val)
			erase(it);
	}
}


// print out all elements. ofc is deliminitor
template <typename T>
void List<T>::print(std::ostream& os, char ofc) const
{
	typename List<T>::const_iterator x = this -> begin();
	for(; x != this -> end(); x++)
		os << *x << ofc;
}



template <typename T>
typename List<T>::iterator List<T>::begin() // iterator to first element
{
	iterator it (head);
	return ++it;
}


template <typename T>
typename List<T>::const_iterator List<T>::begin() const
{
	iterator it (head);
	return ++it;
}


template <typename T>
typename List<T>::iterator List<T>::end() // end marker iterator
{
	iterator it(tail);
	 
	return it;
}


template <typename T>
typename List<T>::const_iterator List<T>::end() const
{	
	const_iterator it(tail);
	return it;
}

template <typename T>
typename List<T>::iterator List<T>::insert(iterator itr, const T& val) // insert val ahead of itr
{	
	Node *p = itr.current;
	theSize++;
	return { p -> prev= p -> prev -> next = new Node { val, p -> prev, p } };
}


template <typename T>
typename List<T>::iterator List<T>::insert(iterator itr, T && val) // move version of insert
{
	Node *p = itr.current;
	theSize++;
	return { p -> prev= p -> prev -> next = new Node { std::move(val), p -> prev, p } };
}
	
template <typename T>
typename List<T>::iterator List<T>::erase(iterator itr) // erase one element
{
	Node *p = itr.current;
	iterator retVal {p -> next};
	p -> prev -> next = p -> next;
	p -> next -> prev = p -> prev;
	delete p;
	theSize--;
	
	return retVal;
}

template <typename T>
typename List<T>::iterator List<T>::erase(iterator start, iterator end) // erase [start, end)
{
	
	for(; start != end; start++)
		erase(start);
	
	return start; 
}

template <typename T>
bool operator==(const List<T> &lhs, const List<T> &rhs)
{
	typename List<T>::const_iterator it = lhs.begin();
	typename List<T>::const_iterator it1 = rhs.begin();
	
	if(lhs.size() != rhs.size())
		return false;
	
	for( ; it != lhs.end() && it1 != rhs.end();++it, ++it1)
	{
		if(*it != *it1)
			return false; 
	}
	
	return true;
}

template <typename T>
bool operator!=(const List<T> & lhs, const List<T> &rhs)
{
	return !(lhs  == rhs );
}
	

// overloading output operator
template <typename T>
std::ostream & operator<<(std::ostream &os, const List<T> &l)
{
	l.print(os);
	
	return os;
}	

