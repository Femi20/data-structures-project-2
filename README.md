### Objectives ###
			
			Understanding generic programming and information hiding by developing generic containers. 
			Getting familiar with the concept of class template and its usage.
			Use of nested (iterator) classes. Use of namespace. 
			Operator overloading. 

### Statement of Work ###
			
			Implement a doubly-linked list class template List and its associated iterators


### How do I get set up? ###

			To create executable: Type 'make proj2.x'
			To run: Type 'proj2.x'
			