#CXX=g++47
CXXFLAGS=-std=c++11 -o

.SUFFIXES: .x

.PHONY: clean


clean:
	rm -f *.o *.x core.*

proj2.x: test_list.cpp List.h List.hpp
	$(CXX) $(CXXFLAGS) $@ $<
	

	
